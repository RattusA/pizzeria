using System.Collections.Generic;

namespace Pizzeria.Pizzayolo
{
    public interface IPizzayolo
    {
        string Id { get; }
        int Level { get; }
        int HallId { get; }
        int PlaceIndex { get; }
        IList<string> PizzaIdList { get; }

        void IncreaseLevel();

        void Activate();
    }
}