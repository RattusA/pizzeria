using System.Collections.Generic;
using JetBrains.Annotations;

namespace Pizzeria.Pizzayolo
{
    public class Pizzayolo : IPizzayolo
    {
        public string Id { get; set; }
        public int Level { get; set; }
        public int HallId { get; }
        public int PlaceIndex { get; }
        public IList<string> PizzaIdList { get; }
        
        public void IncreaseLevel()
        {
            ++Level;
        }

        public void Activate()
        {
            throw new System.NotImplementedException();
        }

        public Pizzayolo([CanBeNull]string id, int level, [NotNull]IList<string> pizzaIdList, int hallId, int placeIndex)
        {
            Id = id;
            Level = level;
            PizzaIdList = pizzaIdList;
            HallId = hallId;
            PlaceIndex = placeIndex;
        }
    }
}