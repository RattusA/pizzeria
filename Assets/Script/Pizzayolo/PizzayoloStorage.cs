using System.Collections.Generic;
using System.Linq;
using Pizzeria.Core;
using Pizzeria.Managers;
using Pizzeria.Pizza;
using Pizzeria.PizzaLevel;
using Pizzeria.Repository;

namespace Pizzeria.Pizzayolo
{
    public class PizzayoloStorage: IStorageManager, ILevelManager, IPizzayoloStorage
    {
        public BaseContainer Storage { get; }

        public PizzayoloStorage()
        {
            Storage = new Container();
            Storage.Put(new Pizzayolo("0", 0, new List<string> {"Regina"}, 0, 0), "0");
            Storage.Put(new Pizzayolo("1", 0, new List<string> {"4"}, 0, 1), "1");
        }
        
        public void IncreaseLevel(object uid)
        {
            var id = (string) uid;
            var data = Storage.Get<string, IPizzayolo>(id);
            data.IncreaseLevel();
            Storage.Put(data, id);
        }

        public IEnumerable<IPizzaLevel> GetSortedPizzaLevelList(object uid)
        {
            var id = (string) uid;
            var data = Storage.Get<string, IPizzayolo>(id);
            var configuration = Configuration.Instance;
            var levelStorage = configuration.PizzaLevel.Storage;
            var pizzaStorage = configuration.PizzaStorage.Storage;
            var pizzaList = new List<IPizza>();
            foreach (var pizzaId in data.PizzaIdList)
            {
                pizzaList.Add(pizzaStorage.Get<string, IPizza>(pizzaId));
            }

            return pizzaList.Where(item => item != null).OrderBy(item => item.Cost).Select(item => levelStorage.Get<string, IPizzaLevel>((item as IUnique)?.Id));
        }
    }

    public interface IPizzayoloStorage
    {
        IEnumerable<IPizzaLevel> GetSortedPizzaLevelList(object uid);
    }
}