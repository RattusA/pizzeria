using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Pizzeria.Pizza;
using Pizzeria.Repository;
using UnityEngine;

namespace Pizzeria.Pizzayolo
{
    public class PizzayoloController : MonoBehaviour, IContainerListener
    {
        [SerializeField] private string _pizzayoloId;
        [SerializeField] private float _placeAnimationPosition; // Example: 7.5f / 300 (total animation frame = 300, spawn frame = 7.5)
        
        private int _plateCount = 0;
        private Coroutine _coroutine;
        protected float cookingTimeout = 10f;

        protected IEnumerator CookingCycle()
        {
            yield return new WaitForSeconds(cookingTimeout);
            yield return new WaitWhile(IsPlaceOccupied);
            var plate = BakePizza();
            Configuration.Instance.PlateSpawnInstance.AddObject(plate, _placeAnimationPosition);
            Debug.Log("Pizza was baked");
            _coroutine = StartCoroutine(CookingCycle());
        }

        protected virtual void OnEnable()
        {
            var storage = Configuration.Instance.PizzayoloStorage.Storage;
            var pizzayolo = storage.Get<string, IPizzayolo>(_pizzayoloId);
            if (pizzayolo != null)
            {
                UpdateView(pizzayolo);
            }

            storage.SubscriberManager.Subscribe(this);
            _coroutine = StartCoroutine(CookingCycle());
        }

        /// <summary>
        /// Update pizzayolo view
        /// </summary>
        /// <param name="pizzayoloData"></param>
        protected virtual void UpdateView(IPizzayolo pizzayoloData)
        {
        }

        protected virtual void OnDisable()
        {
            Configuration.Instance.PizzayoloStorage.Storage.SubscriberManager.Unsubscribe(this);
            if(_coroutine != null) StopCoroutine(_coroutine);
        }

        #region IContainerListener

        public void ObjectWasAdded<TKey, TValue>(object sender, TKey key, TValue value)
        {
            var keyValue = key as string;
            if (keyValue == null || keyValue != _pizzayoloId) return;
            var pizzayolo = value as IPizzayolo;
            if (pizzayolo == null) return;
            UpdateView(pizzayolo);
        }

        public void ObjectWasModified<TKey, TValue>(object sender, TKey key, TValue value)
        {
            var keyValue = key as string;
            if (keyValue == null || keyValue != _pizzayoloId) return;
            var pizzayolo = value as IPizzayolo;
            if (pizzayolo == null) return;
            UpdateView(pizzayolo);
        }

        public void ObjectWasRemoved<TKey>(object sender, TKey key)
        {
            Destroy(gameObject);
        }

        public void ObjectDidError<TKey, TError>(object sender, TKey key, TError error)
        {
            if (error == null) return;
            
            var exception = error as Exception;
            if (exception != null)
            {
                Debug.LogException(exception);
                return;
            }

            var errorMessage = error as string;
            if (errorMessage != null)
            {
                Debug.LogError(errorMessage);
                return;
            }

            Debug.LogError("Unknown error type " + error.GetType());
        }

        #endregion

        [UsedImplicitly]
        public IPlate BakePizza()
        {
            var configuration = Configuration.Instance;
            var storage = configuration.PizzayoloStorage as IPizzayoloStorage;
            var pizzaLevelList = storage.GetSortedPizzaLevelList(_pizzayoloId).ToList();
            var pizzaIdList = new List<string>();
            for (var index = pizzaLevelList.Count - 1; index >= 0; index--)
            {
                var pizzaLevel = pizzaLevelList[index];
                var pizzaId = pizzaLevel.PizzaId;
                if (pizzaLevel.Level == 0) continue;
                var pizza = configuration.PizzaStorage.Storage.Get<string, IPizza>(pizzaId);
                if (pizza == null)
                {
                    Debug.Log("Unknown pizza with id " + pizzaId);
                    continue;
                }

                for (var pieceIndex = 0; pieceIndex < pizzaLevel.Level && pizzaIdList.Count < PlateFourPieces.PIECE_COUNT; pieceIndex++)
                {
                    pizzaIdList.Add(pizzaId);
                }
            }

            return configuration.PizzaCreator.Create(pizzaIdList);
        }

        protected bool IsPlaceOccupied()
        {
            return _plateCount > 0;
        }

        [UsedImplicitly]
        public void OnPlaceWasOccupied()
        {
            _plateCount++;
        }
        
        [UsedImplicitly]
        public void OnPlaceWasRelease()
        {
            _plateCount--;
        }
    }
}