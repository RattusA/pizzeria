using UnityEngine;

namespace Pizzeria
{
    public interface IConveyor
    {
        GameObject Add(GameObject item, ConveyorItemType itemType, float normalizedConveyorPosition);
    }
}