﻿using JetBrains.Annotations;
using Pizzeria.Events;
using UnityEngine;

namespace Pizzeria.View
{
    public class PlateTriggerController : MonoBehaviour
    {
        [SerializeField] protected TriggerEvent triggerEnterEvent;
        [SerializeField] protected TriggerEvent triggerExitEvent;
        
        [UsedImplicitly]
        private void OnTriggerEnter2D(Collider2D other)
        {
            triggerEnterEvent?.Invoke(other);
        }

        [UsedImplicitly]
        private void OnTriggerExit2D(Collider2D other)
        {
            triggerExitEvent?.Invoke(other);
        }
    }
}
