using System.Globalization;
using Pizzeria.Game;
using Pizzeria.Repository;
using UnityEngine;
using UnityEngine.UI;

namespace Pizzeria.View
{
    public class MoneyViewController: MonoBehaviour, IContainerListener
    {
        [SerializeField] protected Text moneyCountLabel;

        #if UNITY_EDITOR
        private void OnValidate()
        {
            Debug.Assert(moneyCountLabel != null);
        }
        #endif

        private void OnEnable()
        {
            var storage = Configuration.Instance.GameStateStorage;
            storage.Storage.SubscriberManager.Subscribe(this);
            UpdateLabel(((IGameStorage) storage).GetMoney());
        }

        private void OnDisable()
        {
            var storage = Configuration.Instance.GameStateStorage;
            storage.Storage.SubscriberManager.Unsubscribe(this);
        }

        public void ObjectWasAdded<TKey, TValue>(object sender, TKey key, TValue value)
        {
            var gameState = value as GameState;
            if (gameState == null) return;
            UpdateLabel(gameState.Money);
        }

        public void ObjectWasModified<TKey, TValue>(object sender, TKey key, TValue value)
        {
            var gameState = value as GameState;
            if (gameState == null) return;
            UpdateLabel(gameState.Money);
        }

        public void ObjectWasRemoved<TKey>(object sender, TKey key)
        {
            // do nothing
        }

        public void ObjectDidError<TKey, TError>(object sender, TKey key, TError error)
        {
            // do nothing
        }

        protected virtual void UpdateLabel(double value)
        {
            moneyCountLabel.text = value.ToString("F0", CultureInfo.InvariantCulture);
        }
    }
}