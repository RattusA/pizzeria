﻿using System.Linq;
using JetBrains.Annotations;
using Pizzeria.PizzaPiece;
using UnityEngine;
using UnityEngine.UI;

namespace Pizzeria
{
    public class PlateWithPizza : MonoBehaviour, IPlateController
    {
        [SerializeField] private Image[] _pieceObjects;
        private IPlate _plateData;
        private static readonly Color _transparentColor = new Color(1, 1, 1, 0);
        private static readonly Color _opaqueColor = Color.white;

        public void SetPlate(IPlate plate)
        {
            _plateData = plate;
            UpdateView();
        }

        private void UpdateView()
        {
            ResetView();
            var pieceList = _plateData.GetPieces();
            for (var pieceIndex = 0; pieceIndex < _pieceObjects.Length && pieceIndex < pieceList.Length; pieceIndex++)
            {
                // TODO: change texture
                if (pieceList[pieceIndex] == null) continue;
                var pieceObject = _pieceObjects[pieceIndex];
                pieceObject.color = _opaqueColor;
            }
        }

        private void ResetView()
        {
            foreach (var pieceObject in _pieceObjects)
            {
                pieceObject.color = _transparentColor;
            }
        }

        public (GameObject, IPizzaPiece)? GetPizzaPiece()
        {
            var piece = _plateData.GetPieces()?.Select((data, index) => new {data, index})
                .FirstOrDefault(item => item.data != null);
            if (piece == null)
            {
                Configuration.Instance.PlateSpawnInstance.ReturnObject(gameObject);
                return null;
            }
            var pieceObject = _pieceObjects[piece.index];
            var pieceObjectClone = Instantiate(pieceObject.gameObject, pieceObject.transform.parent, true);
            pieceObjectClone.transform.localScale = Vector3.one;
            _plateData.GetPieces()[piece.index] = null;
            UpdateView();
            return (pieceObjectClone, piece.data);
        }

    }

    public interface IPlateController
    {
        void SetPlate([NotNull] IPlate plate);

        (GameObject, IPizzaPiece)? GetPizzaPiece();
    }
}