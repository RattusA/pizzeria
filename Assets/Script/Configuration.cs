using Pizzeria.Game;
using Pizzeria.Managers;
using Pizzeria.Pizza;
using Pizzeria.PizzaLevel;
using Pizzeria.Pizzayolo;
using UnityEngine;

namespace Pizzeria
{
    public class Configuration : MonoBehaviour
    {
        public IPizzaCreator PizzaCreator { get; set; }
        public IStorageManager PizzaLevel { get; set; }
        public IStorageManager PizzayoloStorage { get; set; }
        public IStorageManager PizzaStorage { get; set; }
        
        public IStorageManager GameStateStorage { get; set; }

        public PlateSpawn PlateSpawnInstance
        {
            get => _plateSpawnInstance;
            set => _plateSpawnInstance = value;
        }

        private static Configuration _instance;
        [SerializeField] private PlateSpawn _plateSpawnInstance;

        public static Configuration Instance => _instance;

        private void Awake()
        {
            if (_instance)
            {
                Destroy(gameObject);
                return;
            }

            _instance = this;
            Initialize();
        }

        private static void Initialize()
        {
            Instance.GameStateStorage = new GameStateStorage();
            Instance.PizzaLevel = new PizzaLevelStorage();
            Instance.PizzayoloStorage = new PizzayoloStorage();
            Instance.PizzaStorage = new PizzaStorage();
            Instance.PizzaCreator = new PizzaCreator((IPizzaBriefStorage) Instance.PizzaStorage);
        }
    }
}