using Pizzeria.PizzaPiece;

namespace Pizzeria
{
    public interface IPlate
    {
        IPizzaPiece[] GetPieces();
        double Cost();
        IPizzaPiece GetPizzaPiece();
    }
}