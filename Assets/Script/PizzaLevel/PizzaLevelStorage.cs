using Pizzeria.Managers;
using Pizzeria.Repository;

namespace Pizzeria.PizzaLevel
{
    public class PizzaLevelStorage : IStorageManager
    {
        public BaseContainer Storage { get; }

        public PizzaLevelStorage()
        {
            Storage = new Container();
            Storage.Put(new PizzaLevel("Regina", 4, 4), "Regina");
            Storage.Put(new PizzaLevel("1", 0, 4), "1");
        }

        public void AddLevel(object uid)
        {
            var pizzaId = (string) uid;
            var data = Storage.Get<string, IPizzaLevel>(pizzaId);
            data.AddLevel();
            Storage.Put(data, pizzaId);
        }
    }
}