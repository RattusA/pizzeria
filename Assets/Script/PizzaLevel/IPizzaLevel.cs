namespace Pizzeria.PizzaLevel
{
    public interface IPizzaLevel
    {
        string PizzaId { get; }
        int Level { get; }
        int MaxLevel { get; }

        void AddLevel();
    }
    
    public interface IPizzaLevelMutable : IPizzaLevel
    {
        string PizzaId { get; set; }
        int Level { get; set; }
        int MaxLevel { get; set; }
    }
}