using JetBrains.Annotations;

namespace Pizzeria.PizzaLevel
{
    public class PizzaLevel : IPizzaLevelMutable
    {
        public string PizzaId { get; set; }
        public int Level { get; set; }
        public int MaxLevel { get; set; }
        
        public void AddLevel()
        {
            if (Level == MaxLevel) return;
            ++Level;
        }

        public PizzaLevel([CanBeNull]string pizzaId, int level, int maxLevel)
        {
            PizzaId = pizzaId;
            Level = level;
            MaxLevel = maxLevel;
        }
    }
}