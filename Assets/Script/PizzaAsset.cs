﻿using System.Collections.Generic;
using Pizzeria.Pizza;
using UnityEngine;

namespace Pizzeria
{
    [CreateAssetMenuAttribute(fileName = "NewPizzaAsset", menuName = "Pizzeria/PizzaAsset")]
    public class PizzaAsset : ScriptableObject
    {
        public List<PizzaBrief> PizzaBriefs = new List<PizzaBrief>();

    }
}
