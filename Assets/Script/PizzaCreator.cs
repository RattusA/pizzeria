using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace Pizzeria
{
    public class PizzaCreator : IPizzaCreator
    {
        private IPizzaBriefStorage _storage;

        public PizzaCreator([NotNull] IPizzaBriefStorage storage)
        {
            _storage = storage;
        }

        public IPlate Create(IList<string> pizzaIdList)
        {
            var pieceCount = pizzaIdList.Count;
            IPlate plate = null;
            switch (pieceCount)
            {
                case 4:
                case 3:
                case 2:
                case 1:
                    var pizzaList = new PizzaPiece.PizzaPiece[PlateFourPieces.PIECE_COUNT];
                    for (int index = 0; index < pieceCount && index < PlateFourPieces.PIECE_COUNT; index++)
                    {
                        pizzaList[index] = new PizzaPiece.PizzaPiece(_storage.GetPizza(pizzaIdList[index]), PlateFourPieces.PIECE_COUNT);
                    }

                    plate = new PlateFourPieces(pizzaList);
                    break;
                case 0:
                    Debug.LogWarning("Plate can't be empty");
                    break;
                default:
                    Debug.LogWarning("Plate can't contain " + pieceCount + " pieces of pizza");
                    break;
            }

            return plate;
        }
    }
}