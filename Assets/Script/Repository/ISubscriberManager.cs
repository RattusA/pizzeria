using System;
using JetBrains.Annotations;

namespace Pizzeria.Repository
{
	public interface ISubscriberManager
	{
		void Subscribe([NotNull] IContainerListener listener);

		void Unsubscribe([NotNull] IContainerListener listener);

		void ObjectWasAdded<TKey, TValue>([NotNull] object sender, [CanBeNull] TKey key, [NotNull] TValue value);

		void ObjectWasModified<TKey, TValue>([NotNull] object sender, [CanBeNull] TKey key, [NotNull] TValue value);

		void ObjectWasRemoved<TKey>([NotNull] object sender, [CanBeNull] TKey key);

		void ObjectDidError<TKey, TError>([NotNull] object sender, [CanBeNull] TKey key, [CanBeNull] TError error);
	}
}