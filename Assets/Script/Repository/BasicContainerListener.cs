namespace Pizzeria.Repository
{
    public abstract class BasicContainerListener : IContainerListener
    {
        public virtual void ObjectWasAdded<TKey, TValue>(object sender, TKey key, TValue value)
        {
            // nothing to do	
        }

        public virtual void ObjectWasModified<TKey, TValue>(object sender, TKey key, TValue value)
        {
            // nothing to do	
        }

        public virtual void ObjectWasRemoved<TKey>(object sender, TKey key)
        {
            // nothing to do	
        }

        public virtual void ObjectDidError<TKey, TError>(object sender, TKey key, TError error)
        {
            // nothing to do	
        }
    }
}