using System;
using System.Collections;
using System.Collections.Generic;

namespace Pizzeria.Repository
{
    public abstract class BaseContainer : IContainer
    {
        private Hashtable _hashtable = new Hashtable();
        private readonly ISubscriberManager _subsciberManager;

        public BaseContainer()
        {
            _subsciberManager = new SubscriberManager();
        }
        
        protected Hashtable Data
        {
            get { return _hashtable; }
        }

        public ISubscriberManager SubscriberManager
        {
            get { return _subsciberManager; }
        }

        public abstract bool Contains<TKey>(TKey key);


        public virtual void ClearAll()
        {
            Data.Clear();
        }

        public abstract void Put<TKey, TValue>(TValue value, TKey key);

        public abstract TValue Get<TKey, TValue>(TKey key);

        public abstract void Remove<TKey, TValue>(TKey key = default(TKey));

        public abstract IEnumerable<TKey> Query<TKey, TQuery>(TQuery query = default(TQuery));
    }
}