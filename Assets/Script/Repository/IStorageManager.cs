using Pizzeria.Repository;

namespace Pizzeria.Managers
{
    public interface IStorageManager
    {
        BaseContainer Storage { get; }
    }
}