using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Pizzeria.Repository
{
	public interface IContainer
	{
		bool Contains<TKey>([CanBeNull] TKey key);

		void ClearAll();

		void Put<TKey, TValue>([NotNull] TValue value, [CanBeNull] TKey key);

		TValue Get<TKey, TValue>([CanBeNull] TKey key);

		void Remove<TKey, TValue>([CanBeNull] TKey key = default(TKey));

		[NotNull]
		IEnumerable<TKey> Query<TKey, TQuery>([CanBeNull] TQuery query = default(TQuery));
	}
}