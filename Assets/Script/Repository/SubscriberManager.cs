using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzeria.Repository
{
    public class SubscriberManager: ISubscriberManager
    {
        private readonly IList<IContainerListener> _subscribers = new List<IContainerListener>();
        
        public void Subscribe(IContainerListener listener)
        {
            _subscribers.Add(listener);
        }

        public void Unsubscribe(IContainerListener listener)
        {
            _subscribers.Remove(listener);
        }

        public void ObjectWasAdded<TKey, TValue>(object sender, TKey key, TValue value)
        {
            Notify(listener => listener.ObjectWasAdded(sender, key, value));
        }

        public void ObjectWasModified<TKey, TValue>(object sender, TKey key, TValue value)
        {
            Notify(listener => listener.ObjectWasModified(sender, key, value));
        }

        public void ObjectWasRemoved<TKey>(object sender, TKey key)
        {
            Notify(listener => listener.ObjectWasRemoved(sender, key));
        }

        public void ObjectDidError<TKey, TError>(object sender, TKey key, TError error)
        {
            Notify(listener => listener.ObjectDidError(sender, key, error));
        }

        private void Notify(Action<IContainerListener> action)
        {
            var clonedList = _subscribers.ToList();
            clonedList.ForEach(action.Invoke);
        }

    }
}