using System.Collections.Generic;

namespace Pizzeria.Repository
{
    public class Container: BaseContainer
    {
        public override bool Contains<TKey>(TKey key)
        {
            return Data.ContainsKey(key);
        }

        public override void Put<TKey, TValue>(TValue value, TKey key)
        {
            var wasExists = Contains(key);
            Data[key] = value;
            if (wasExists)
                SubscriberManager.ObjectWasModified(this, key, value);
            else
                SubscriberManager.ObjectWasAdded(this, key, value);
        }

        public override TValue Get<TKey, TValue>(TKey key)
        {
            var result = Data[key];
            return  result is TValue value ? value : default(TValue);
        }

        public override void Remove<TKey, TValue>(TKey key = default(TKey))
        {
            Data.Remove(key);
            SubscriberManager.ObjectWasRemoved(this, key);
        }

        public override IEnumerable<TKey> Query<TKey, TQuery>(TQuery query = default(TQuery))
        {
            var keys = new TKey[Data.Keys.Count];
            Data.Keys.CopyTo(keys, 0);
            return keys;
        }
    }
}