using System;
using JetBrains.Annotations;

namespace Pizzeria.Repository
{
	public interface IContainerListener
	{
		void ObjectWasAdded<TKey, TValue>([NotNull] [UsedImplicitly] object sender, [CanBeNull] TKey key, [NotNull] TValue value);

		void ObjectWasModified<TKey, TValue>([NotNull] [UsedImplicitly] object sender, [CanBeNull] TKey key, [NotNull] TValue value);

		void ObjectWasRemoved<TKey>([NotNull] [UsedImplicitly] object sender, [CanBeNull] TKey key);

		void ObjectDidError<TKey, TError>([NotNull] [UsedImplicitly] object sender, [CanBeNull] TKey key, [CanBeNull] TError error);
	}
}