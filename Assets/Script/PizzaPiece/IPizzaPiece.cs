using Pizzeria.Pizza;

namespace Pizzeria.PizzaPiece
{
    public interface IPizzaPiece: IPizza
    {
        IPizza Pizza { get; }
        new double Cost { get; }
        
        /// <summary>
        ///  ratio of size to the entire piece of pizza
        /// </summary>
        float PieceSize { get; }
    }
    
    public interface IPizzaPieceMutable: IPizzaPiece
    {
        new IPizza Pizza { get; set; }
        new float PieceSize { get; set; }
    }
}