using Pizzeria.Pizza;

namespace Pizzeria.PizzaPiece
{
    public class PizzaPiece: IPizzaPieceMutable
    {
        /// <summary>
        ///  ratio of size to the entire piece of pizza
        /// </summary>
        public float PieceSize { get; set; }
        public IPizza Pizza { get; set; }
        public double Cost => Pizza.Cost * PieceSize;

        public PizzaPiece(IPizza pizza, int pieceCount)
        {
            Pizza = pizza;
            PieceSize = 1f / pieceCount;
        }
    }
}