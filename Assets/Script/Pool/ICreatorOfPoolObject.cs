﻿using UnityEngine;

namespace Pizzeria.Pool
{
	public interface ICreatorOfPoolObject
	{
		GameObject CreatePoolObject();
	}
}
