﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace Pizzeria.Pool
{
	[Serializable]
	public sealed class PoolController
	{
		private readonly Queue<GameObject> _pool;
		private readonly Transform _rootTransform;
		private readonly ICreatorOfPoolObject _parent;

		public PoolController([NotNull]ICreatorOfPoolObject parent, [NotNull]Transform rootTransform)
		{
			_pool = new Queue<GameObject>();
			_parent = parent;
			_rootTransform = rootTransform;
		}

		public GameObject GetObject()
		{
			if (_pool.Count > 0)
				return _pool.Dequeue();
			else
				return _parent.CreatePoolObject();
		}

		public void ReturnObject(Transform go)
		{
			_pool.Enqueue(go.gameObject);
			go.SetParent(_rootTransform, false);
		}
	}
}
