using System.Linq;
using Pizzeria.PizzaPiece;

namespace Pizzeria
{
    public class PlateFourPieces: IPlate
    {
        public const int PIECE_COUNT = 4;
        private readonly IPizzaPiece[] _pieces = new IPizzaPiece[PIECE_COUNT];

        public PlateFourPieces(IPizzaPiece[] pizzaPieces)
        {
            for (var index = 0; index < pizzaPieces.Length && index < PIECE_COUNT; index++)
            {
                _pieces[index] = pizzaPieces[index];
            }
        }

        public IPizzaPiece[] GetPieces()
        {
            return _pieces;
        }

        public double Cost()
        {
            return _pieces.Where(piece => piece != null).Sum(piece => piece.Cost);
        }

        public IPizzaPiece GetPizzaPiece()
        {
            IPizzaPiece piece = null;
            for (var index = 0; index < _pieces.Length; index++)
            {
                piece = _pieces[index];
                if (piece == null) continue;
                _pieces[index] = null;
                break;
            }

            return piece;
        }
    }
}