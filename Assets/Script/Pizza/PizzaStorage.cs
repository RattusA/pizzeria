using Pizzeria.Managers;
using Pizzeria.Repository;

namespace Pizzeria.Pizza
{
    public class PizzaStorage: IStorageManager, IPizzaBriefStorage
    {
        public BaseContainer Storage { get; }

        public PizzaStorage()
        {
            Storage = new Container();
            Storage.Put(new PizzaBrief("Regina", "Regina", 7), "Regina");
        }

        public IPizza GetPizza(string id)
        {
            var pizza = Storage.Get<string, IPizza>(id);
            return pizza;
        }
    }
}