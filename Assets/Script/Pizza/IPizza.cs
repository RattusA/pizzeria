namespace Pizzeria.Pizza
{
    public interface IPizza
    {
        double Cost { get; }
    }
    
    public interface IPizzaMutable: IPizza
    {
        new double Cost { get; set; }
    }
}