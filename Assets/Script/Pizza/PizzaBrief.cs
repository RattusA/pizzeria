﻿using System;
using Pizzeria.Core;
using UnityEngine;

namespace Pizzeria.Pizza
{
    public class PizzaBrief : ScriptableObject, IPizzaMutable, IUniqueMutable, INamedMutable
    {
        [HideInInspector] [SerializeField] private string _id;
        [SerializeField] private string _name;
        [SerializeField] private double _cost;

        public string Id
        {
            get => _id;
            set => _id = value;
        }

        public string Name
        {
            get => _name;
            set => _name = value;
        }

        public double Cost
        {
            get => _cost;
            set => _cost = value;
        }

        public PizzaBrief()
        {
            Id = Guid.NewGuid().ToString();
            Name = "Unknown pizza";
            Cost = 0;
        }

        public PizzaBrief(string name, double cost)
        {
            Id = Guid.NewGuid().ToString();
            Name = name;
            Cost = cost;
        }
        
        public PizzaBrief(string id, string name, double cost)
        {
            Id = id;
            Name = name;
            Cost = cost;
        }
    }
}
