﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace Pizzeria
{
    public class Conveyor : MonoBehaviour, IConveyor
    {
        private const string _MOVEMENT_ANIMATION_NAME = "ConveyorBeltMovement";
        private const float _BASE_SPEED = 0.14f;
        
        [SerializeField] private GameObject _conveyorSegment;
        [SerializeField] private Transform _beltContainer;
        [SerializeField] private Transform _beltObjectsContainer;
        [SerializeField] private int _segmentCount = 50;
        [SerializeField] private float _speed = 1f;
        
        private readonly List<Animator> _conveyorObjectList = new List<Animator>();

#if UNITY_EDITOR
        private void OnValidate()
        {
            Debug.Assert(_conveyorSegment != null, "Conveyor segment not assigned");
            Debug.Assert(_beltContainer != null);
            Debug.Assert(_beltObjectsContainer != null);
        }
#endif

        // Start is called before the first frame update
        private void Start()
        {
            for (var index = 0; index < _segmentCount; index++)
            {
                var beltSegmentObject = Instantiate(_conveyorSegment);
                Add(beltSegmentObject, ConveyorItemType.Belt, (float) index / _segmentCount);
            }
        }

        // Update is called once per frame
        private void Update()
        {
            foreach (var segment in _conveyorObjectList)
            {
                segment.speed = _BASE_SPEED * _speed;
            }
        }

        public GameObject Add([NotNull] GameObject conveyorObject, ConveyorItemType itemType, float normalizedConveyorPosition)
        {
            conveyorObject.transform.SetParent(GetContainerByType(itemType), false);
            conveyorObject.SetActive(true);
            var animator = conveyorObject.GetComponent<Animator>();
            if (!animator.enabled)
                animator.enabled = true;
            _conveyorObjectList.Add(animator);
            animator.speed = _BASE_SPEED * _speed;
            animator.Play(_MOVEMENT_ANIMATION_NAME, 0, normalizedConveyorPosition);
            return conveyorObject;
        }

        [NotNull]
        private Transform GetContainerByType(ConveyorItemType itemType)
        {
            switch (itemType)
            {
                case ConveyorItemType.Belt:
                    return _beltContainer;
                case ConveyorItemType.BeltObject:
                    return _beltObjectsContainer;
            }

            return _beltObjectsContainer;
        }
    }

    public enum ConveyorItemType
    {
        Belt,
        BeltObject
    }
}