namespace Pizzeria.Core
{
	public interface IUnique
	{
		string Id { get; }
	}

	public interface IUniqueMutable: IUnique
	{
		new string Id { get; set; }
	}
}