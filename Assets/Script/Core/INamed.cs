namespace Pizzeria.Core
{
	public interface INamed
	{
		string Name { get; }
	}

	public interface INamedMutable: INamed
	{
		new string Name { get; set; }
	}
}