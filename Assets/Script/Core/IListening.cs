namespace Pizzeria.Core
{
    public interface IListening<in T>
    {
        void Subscribe(T listener);
        void Unsubscribe(T listener);
    }
}