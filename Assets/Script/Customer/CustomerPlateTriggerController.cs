using JetBrains.Annotations;
using Pizzeria.Events;
using UnityEngine;

namespace Pizzeria.Customer
{
    public class CustomerPlateTriggerController: MonoBehaviour
    {
        [SerializeField] protected TriggerEvent triggerStayEvent;

        [UsedImplicitly]
        private void OnTriggerStay2D(Collider2D other)
        {
            triggerStayEvent?.Invoke(other);
        }
    }
}