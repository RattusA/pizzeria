namespace Pizzeria.Customer
{
    public class Customer : ICustomer
    {
        public Customer()
        {
            ComesInSpeed = 500f;
            ComesOutSpeed = 200f;
            PlateMovementSpeed = 500f;
            EatSpeed = 80f;
            IdleTime = 5f;
            MaxPizzaEatCount = 2;
        }

        public float ComesInSpeed { get; }
        public float ComesOutSpeed { get; }
        public float PlateMovementSpeed { get; }
        public float EatSpeed { get; }
        public float IdleTime { get; }
        public int MaxPizzaEatCount { get; }
    }
}