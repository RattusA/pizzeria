using UnityEngine;

namespace Pizzeria.Customer
{
    public interface ICustomerController
    {
        void ReceiveConveyorObject(GameObject otherGameObject);
    }
}