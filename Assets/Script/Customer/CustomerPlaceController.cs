﻿using JetBrains.Annotations;
using Pizzeria.Core;
using UnityEngine;

namespace Pizzeria.Customer
{
    public class CustomerPlaceController : MonoBehaviour, IListening<ICustomerController>, ICustomerPlaceController
    {
        [SerializeField] private GameObject _triggerObject;
        [SerializeField] private Transform _customerSpawn;
        [SerializeField] private Transform _sitPlace;
        [SerializeField] private Transform _platePlace;
        [SerializeField] private GameObject _customer;

        public Transform CustomerSpawnPoint => _customerSpawn;
        public Transform SitPlace => _sitPlace;
        public Transform PlatePlace => _platePlace;
        public GameObject CustomerObject => _customer;

        private ICustomerController _listener;
        
        #if UNITY_EDITOR
        private void OnValidate()
        {
            Debug.Assert(_triggerObject != null);
            Debug.Assert(_customerSpawn != null);
            Debug.Assert(_sitPlace != null);
            Debug.Assert(_platePlace != null);
            Debug.Assert(_customer != null);
        }
        #endif

        public void Subscribe(ICustomerController listener)
        {
            _listener = listener;
        }

        public void Unsubscribe(ICustomerController listener)
        {
            _listener = null;
        }

        [UsedImplicitly]
        public void OnObjectStayOnTrigger(Collider2D other)
        {
            _listener?.ReceiveConveyorObject(other.gameObject);
        }
    }
}