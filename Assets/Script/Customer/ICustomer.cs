namespace Pizzeria.Customer
{
    public interface ICustomer
    {
        float ComesInSpeed { get; }
        float ComesOutSpeed { get; }
        float PlateMovementSpeed { get; }
        float EatSpeed { get; }
        /// <summary>
        /// In seconds
        /// </summary>
        float IdleTime { get; }
        int MaxPizzaEatCount { get; }
    }
}