using System.Collections;
using Pizzeria.Core;
using Pizzeria.Game;
using UnityEngine;

namespace Pizzeria.Customer
{
    public class CustomerController: MonoBehaviour, ICustomerController
    {
        [SerializeField] private GameObject _customerPlaceController;
        [SerializeField] private float _comesInSpeed = 0.2f;
        [SerializeField] private float _comesOutSpeed = 0.1f;
        [SerializeField] private float _plateMovementSpeed = 0.1f;
        [SerializeField] private float _eatSpeed = 0.2f;
        [SerializeField] private float _idleTime = 5f; // seconds
        [SerializeField] private int _maxPizzaEatCount = 2;

        private ICustomer _customerData;
        private GameObject _plate;
        private CustomerStateEnum _state = CustomerStateEnum.ComesIn;
        private Coroutine _coroutine;
        private int _pizzaEatCount;

        public CustomerStateEnum State
        {
            get { return _state; }
            set
            {
                _state = value;
                if(_coroutine != null) StopCoroutine(_coroutine);
                switch (value)
                {
                    case CustomerStateEnum.ComesIn:
                        _coroutine = StartCoroutine(ComesIn());
                        break;
                    case CustomerStateEnum.PlateWaiting:
                        _coroutine = StartCoroutine(PlateWaiting());
                        break;
                    case CustomerStateEnum.GetPlate:
                        _coroutine = StartCoroutine(GetPlate());
                        break;
                    case CustomerStateEnum.Eat:
                        _coroutine = StartCoroutine(Eat());
                        break;
                    case CustomerStateEnum.ComesOut:
                        _coroutine = StartCoroutine(ComesOut());
                        break;
                    case CustomerStateEnum.Idle:
                        _coroutine = StartCoroutine(Idle());
                        break;
                    default: break;
                }
            }
        }

        private IEnumerator Idle()
        {
            yield return new WaitForSeconds(_idleTime);
            State = CustomerStateEnum.ComesIn;
        }

        private IEnumerator Eat()
        {
            var piece = _plate.GetComponent<IPlateController>().GetPizzaPiece();
            if (piece == null)
            {
                _pizzaEatCount++;
                State = _pizzaEatCount == _maxPizzaEatCount ? CustomerStateEnum.ComesOut : CustomerStateEnum.PlateWaiting;
                yield break;
            }

            var pieceObject = piece.Value.Item1;
            var placeController = _customerPlaceController.GetComponent<ICustomerPlaceController>();
            pieceObject.transform.SetParent(placeController.PlatePlace.parent, false);
            
            yield return Move(pieceObject.transform, placeController.SitPlace, _eatSpeed);
            
            var gameState = (IGameStorage) Configuration.Instance.GameStateStorage;
            gameState.AddMoney(piece.Value.Item2.Cost);
            Destroy(pieceObject);
            State = CustomerStateEnum.Eat;
        }

        private IEnumerator GetPlate()
        {
            _plate.transform.SetParent(_customerPlaceController.transform);
            var placeController = _customerPlaceController.GetComponent<ICustomerPlaceController>();
            
            yield return Move(_plate.transform, placeController.PlatePlace, _plateMovementSpeed);

            State = CustomerStateEnum.Eat;
        }

        private IEnumerator PlateWaiting()
        {
            var subscribeManager = _customerPlaceController.GetComponent<IListening<ICustomerController>>();
            if (subscribeManager == null)
            {
                Debug.LogError("customerPlaceController no have component with <IListening<ICustomerController> interface");
                State = CustomerStateEnum.ComesOut;
                yield break;
            }

            subscribeManager.Subscribe(this);
            yield return new WaitWhile(() => _plate == null);
            subscribeManager.Unsubscribe(this);
            State = CustomerStateEnum.GetPlate;
        }

        private IEnumerator ComesOut()
        {
            var placeController = _customerPlaceController.GetComponent<ICustomerPlaceController>();
            RotateZAxis(transform, 180);
            
            yield return Move(transform, placeController.CustomerSpawnPoint, _comesOutSpeed);

            _plate = null;
            State = CustomerStateEnum.Idle;
        }

        private IEnumerator ComesIn()
        {
            if (!gameObject.activeSelf)
            {
                gameObject.SetActive(true);
            }

            var placeController = _customerPlaceController.GetComponent<ICustomerPlaceController>();
            RotateZAxis(transform, 0);

            yield return Move(transform, placeController.SitPlace, _comesInSpeed);

            transform.localPosition = placeController.SitPlace.localPosition;
            State = CustomerStateEnum.PlateWaiting;
            _pizzaEatCount = 0;
        }
        
        public void ReceiveConveyorObject(GameObject otherGameObject)
        {
            _plate = otherGameObject;
            if (_plate == null) return;
            var _plateAnimator = _plate.GetComponent<Animator>();
            _plateAnimator.enabled = false;
        }

        private void Start()
        {
            StartCoroutine(Initialization());
        }

        private IEnumerator Initialization()
        {
            // Waiting of layout initialization
            yield return new WaitForEndOfFrame();
            State = CustomerStateEnum.ComesIn;
        }

        private IEnumerator Move(Transform from, Transform to, float speed)
        {
            var startPosition = from.localPosition;
            var distance = Vector3.Distance(startPosition, to.localPosition);
            var startTime = Time.time;
            while (Vector3.Distance(from.localPosition, startPosition) < distance)
            {
                var distCovered = (Time.time - startTime) * speed;
                var fracJourney = distCovered / distance;
                from.localPosition = Vector3.Lerp(startPosition, to.localPosition, fracJourney);
                yield return new WaitForEndOfFrame();
            }
        }

        private void RotateZAxis(Transform subjectTransform, float angle)
        {
            var rotation = subjectTransform.localRotation;
            var eulerAngles = rotation.eulerAngles;
            eulerAngles.z = angle;
            rotation.eulerAngles = eulerAngles;
            subjectTransform.localRotation = rotation;
        }
    }

    public enum CustomerStateEnum
    {
        Idle,
        ComesIn,
        PlateWaiting,
        GetPlate,
        Eat,
        ComesOut
    }
}