using UnityEngine;

namespace Pizzeria.Customer
{
    public interface ICustomerPlaceController
    {
        Transform CustomerSpawnPoint { get; }
        Transform SitPlace { get; }
        Transform PlatePlace { get; }
    }
}