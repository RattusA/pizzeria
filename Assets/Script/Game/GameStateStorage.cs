using Pizzeria.Managers;
using Pizzeria.Repository;
using UnityEngine;

namespace Pizzeria.Game
{
    public class GameStateStorage: IStorageManager, IGameStorage
    {
        public BaseContainer Storage { get; }
        
        public GameStateStorage()
        {
            Storage = new Container();
            Storage.Put<string, GameState>(new GameState(0), "0");
        }

        public void AddMoney(double moneyChanges)
        {
            if (Mathf.Approximately((float) moneyChanges, 0)) return;
            var state = Storage.Get<string, GameState>("0");
            Storage.Put<string, GameState>(new GameState(state.Money + moneyChanges), "0");
        }

        public double GetMoney()
        {
            return Storage.Get<string, GameState>("0").Money;
        }
    }
}