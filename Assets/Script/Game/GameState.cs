namespace Pizzeria.Game
{
    public class GameState
    {
        protected double money;
        
        public double Money => money;
        
        public GameState(double money)
        {
            this.money = money;
        }
    }
}