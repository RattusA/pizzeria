namespace Pizzeria.Game
{
    public interface IGameStorage
    {
        void AddMoney(double moneyChanges);
        double GetMoney();
    }
}