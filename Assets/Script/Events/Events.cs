using System;
using UnityEngine;
using UnityEngine.Events;

namespace Pizzeria.Events
{
    [Serializable]
    public class TriggerEvent: UnityEvent<Collider2D>{}
}