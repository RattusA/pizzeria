using System.Collections.Generic;

namespace Pizzeria
{
    public interface IPizzaCreator
    {
        IPlate Create(IList<string> pizzaIdList);
    }
}