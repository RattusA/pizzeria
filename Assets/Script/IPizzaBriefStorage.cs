using JetBrains.Annotations;
using Pizzeria.Pizza;

namespace Pizzeria
{
    public interface IPizzaBriefStorage
    {
        [CanBeNull]
        IPizza GetPizza([CanBeNull] string id);
    }
}