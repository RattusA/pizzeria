namespace Pizzeria
{
    public interface ILevelManager
    {
        void IncreaseLevel(object uid);
    }
}