﻿using System;
using Pizzeria.Pool;
using UnityEngine;
using UnityEngine.Serialization;

namespace Pizzeria
{
    public class PlateSpawn : MonoBehaviour, ICreatorOfPoolObject
    {
        [FormerlySerializedAs("_plateView")] [SerializeField] protected GameObject plateView;
        [FormerlySerializedAs("_poolContainer")] [SerializeField] protected Transform poolContainer;
        [FormerlySerializedAs("_conveyor")] [SerializeField] protected GameObject conveyor;
        protected PoolController pool;

#if UNITY_EDITOR
        protected virtual void OnValidate()
        {
            Debug.Assert(plateView != null, "plateView != null");
            Debug.Assert(poolContainer != null, "poolContainer != null");
        }
#endif

        protected virtual void Awake()
        {
            pool = new PoolController(this, poolContainer);
        }

        #region ICreatorOfPoolObject

        public virtual GameObject CreatePoolObject()
        {
            var plate = Instantiate(plateView, poolContainer, true);
            return plate;
        }

        #endregion

        public virtual void AddObject(IPlate plateData, float spawnTime)
        {
            var conveyorController = conveyor.GetComponent<IConveyor>();
            var plateObject = pool.GetObject();
            conveyorController.Add(plateObject, ConveyorItemType.BeltObject, spawnTime);
            plateObject.GetComponent<IPlateController>().SetPlate(plateData);
            plateObject.GetComponent<Animator>().enabled = true;
        }

        public virtual void ReturnObject(GameObject poolObject)
        {
            var objectTransform = poolObject.transform;
            pool.ReturnObject(objectTransform);
            objectTransform.localPosition = Vector3.zero;
        }
    }
}
